/*
EEE174 - Section 4 - THURS. 6:30pm-9:10pm 
Spring 2020
Prof. Nitish Khazane 

GROUP NAME: FOREFINGER PERCUSSIONISTS
MEMBERS: Ricardo Navarrete Jr. & Mark Zaverukha
*/

#include "simpletools.h"
#include "wavplayer.h" //Need to incude 
void beep();
void blink();

int main() // main function
{
int DO = 22, CLK = 23, DI = 24, CS = 25; // SD I/O pins
sd_mount(DO, CLK, DI, CS); // Mount SD card

cog_run(beep,128); //Main code only relies on 2 cogs of the parllax board with a module each
cog_run(blink, 128);
}

void blink() //Module for the code of the LEDs to correspond with pushbuttons
{ while(1)
{
int but1=input(15); int but2=input(13); int but3=input(11); int but4=input(9);
int but5=input(7); int but6=input(5); int but7=input(3); int but8=input(1);

if(but1 == 1) // Start of IF statements for all the LEDs
{high(14);}
if(but2 == 1) // Using HIGH to turn on LED and assigning LEDs to all EVEN PINs on parallax
{high(12);}
if(but3 == 1)
{high(10);}
if(but4 == 1)
{high(8);}
if(but5 == 1)
{high(6);}
if(but6 == 1)
{high(4);}
if(but7 == 1)
{high(2);}
if(but8 == 1)
{high(0);}
else
{low(14); low(12); low(10); low(8); low(6); low(4); low(2); low(0);} //If no buttons pressed, no LED on
}
}

void beep() //Calling the root module of the project with all the wav files
{
while(1)
{
const char piano1[] = {"piano1.wav"};
const char piano2[] = {"piano2.wav"}; // MAKE SURE ALL WAV FILES ARE ONLY EIGHT BITS LONG
const char piano3[] = {"piano3.wav"};
const char piano4[] = {"piano4.wav"};
const char string1[] = {"string1.wav"}; //Parallax only allows 8 bit names, excluding the .wav
const char string2[] = {"string2.wav"};
const char rap140[] = {"rap140.wav"};

int but1 = input(1); //Assign all the pushbuttons for this module, just like in blink module
int but2 = input(3);
int but3 = input(5); //space the 220ohm resistors going into the pins out by 2 slots
int but4 = input(7);
int but5 = input(9); // Pushbuttons with wav files are all ODD PINS on parallax board
int but6 = input(11);
int but7 = input(13);  
int but8 = input(15); //Last button will be our reset button to end all wav files and lights on
pause(200);

if(but1==1) //Start of all IF statements for pushbuttons and wav files playing
{
wav_play(piano1);
wav_volume(8);
high(0);
low(14); low(12); low(10); low(8); low(6); low(4); low(2);
}
if(but2==1)
{
wav_play(piano2);
wav_volume(8);
high(2);
low(14); low(12); low(10); low(8); low(6); low(4); low(0); //LOW code prevents other lights from staying on
}
if(but3==1)
{
wav_play(piano3);
wav_volume(8);
high(4);
low(14); low(12); low(10); low(8); low(6); low(2); low(0);
}
if(but4==1)
{
wav_play(piano4);
wav_volume(8); // WAV VOLUME is set to 8 to play wav files loud enough to be heard
high(6);
low(14); low(12); low(10); low(8); low(4); low(2); low(0);
}
if(but5==1)
{
wav_play(string1);
wav_volume(8);
high(8);
low(14); low(12); low(10); low(6); low(4); low(2); low(0);
}
if(but6==1)
{
wav_play(string2);
wav_volume(8);
high(10);
low(14); low(12); low(8); low(6); low(4); low(2); low(0);
}
if(but7==1)
{
wav_play(rap140);
wav_volume(7);
high(12);
low(14); low(10); low(8); low(6); low(4); low(2); low(0);
}
if(but8==1) // Button 8 is our backup reset button to stop Wav files from playing and LEDs turn off 
{
wav_stop();
low(14); low(12); low(10); low(8); low(6); low(4); low(2); low(0);
pause(600); //pause after reseting for .6 seconds
}
}
}
//END OF CODE